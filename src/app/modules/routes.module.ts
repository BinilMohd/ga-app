import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { LogoutComponent } from '../components/logout/logout.component';
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot([
            {
                path        :   '',
                redirectTo  :   '/login',
                pathMatch   :   'full'
            }, {
                path        :   'login',
                component   :   LoginComponent
            }, {
                path        :   'logout',
                component   :   LogoutComponent
            }, {
                path        :   '**',
                component   :   PageNotFoundComponent
            }
        ])
    ]
})
export class RoutesModule { }
