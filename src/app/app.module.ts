import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from './modules/material.module';
import { RoutesModule } from './modules/routes.module';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        LogoutComponent,
        PageNotFoundComponent
    ],
    imports: [
        BrowserModule,
        FlexLayoutModule,
        MaterialModule,
        RouterModule,
        RoutesModule
    ],
    providers: [],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
